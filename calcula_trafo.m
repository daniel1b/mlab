function saida = calcula_trafo(Poste)
    estrutura = 'A4';
    DimFixas = busca_estruturaSECUN(estrutura);    
    if isequal(Poste.posicao,'normal')
        Baixa.hn = DimFixas.hn_normal;
        Baixa.ha = DimFixas.hfa_normal;
        Baixa.hb = DimFixas.hfb_normal;
        Baixa.hc = DimFixas.hfc_normal;
        Baixa.hd = DimFixas.hfd_normal;        
    else
        Baixa.hn = DimFixas.hfn_topo;
        Baixa.ha = DimFixas.hfa_topo;
        Baixa.hb = DimFixas.hfb_topo;
        Baixa.hc = DimFixas.hfc_topo;
        Baixa.hd = DimFixas.hfd_topo;
    end

    hmaior = Baixa.hn;
    hmenor = Baixa.hn;
    
    fases = {'ha' 'hb' 'hc' 'hd'};
    for i = 1:4 % desenhando o 1 e 2
        if ~isnan(eval(['Baixa.' fases{i}]))
            hmenor = eval(['Baixa.' fases{i}]);
        end
    end
    
    saida = (hmaior + hmenor)/2;
    
    if Poste.neutro<saida
        saida = Poste.neutro;
    end
end