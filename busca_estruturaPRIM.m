%        hc_topo: 0.2000
%        hf_topo: 0.2000
%      hc_normal: 0.1500
%      hf_normal: 0.1500
%           hfn1: 1.2000
%           hfn2: 1.2000
%           hfn3: NaN
%           hfn4: NaN
%           hfn5: NaN
%           hfn6: NaN
%     hpPorFonte: 0.4690
%     hpPorCarga: 0.4690
%     hpPilFonte: NaN
%     hpPilCarga: NaN
%        hestlat: 0.2500
%        hestlon: NaN
%          preco: 111.870



function parametros = busca_estruturaPRIM(estrBuscar,tensao)

    nomeArquivo = 'TABELA_Estr.csv';  
    Arq = fopen(nomeArquivo);
    leitura = textscan(Arq,'%s',19, 'delimiter', ',');
    cabecalho = leitura{:};
    estrutura = cell(86,2);
    for i = 1:98
        leitura = textscan(Arq,'%s',1, 'delimiter', ',');
        estrutura(i,1) = leitura{1};
        leitura = textscan(Arq,'%s',1, 'delimiter', ',');
        estrutura(i,2) = leitura{1};
        for j = 1:17
            textscan(Arq,'%s',1, 'delimiter', ','); 
        end
    end
    fclose(Arq);
    
    linha = -1;
    for i = 1:length(estrutura(:,1))
        if isequal(estrutura{i,1}, estrBuscar)
            if isequal(estrutura{i,2}, tensao)
                linha = i;
                break;
            end
        end
    end
    
    if(linha<0)
        parametros = 'cobinacao nao disponivel';
    else        
        dados = csvread(nomeArquivo, linha,2,[linha 2 linha 18]);
        %dados contem a linha do csv correnspondente a estrutura e a tensao

        for i = 1:length(cabecalho)-2
            eval(strcat('parametros.',cabecalho{i+2},' = ',num2str(dados(i)),';'));    
        end
    end

end