clc; clear; close all;

Mono = {'U1' 'U2' 'U3' 'U4' 'U3-U3' 'U2-3' 'UD-U1' 'UD-U2' 'UD-U4' 'UD-T'};
Tri = {'N1' 'N2' 'N3' 'N4' 'N3-N3' 'N2-3' 'HT' 'HTE' 'ND-N1' ...
    'ND-N2' 'ND-N4' 'P1' 'P1-A' 'P3' 'P4' ...
    'P3-3' 'B1' 'B2' 'B3' 'B4' 'B2-3' 'TE' 'M1' 'M2' 'M3' 'M4'};
Estrutura = horzcat(Mono,Tri);
tensao = {'13.8kV' '34.5kV' 'N-34.5kV'};
campos = {'hc_topo' 'hf_topo' 'hc_normal' 'hf_normal' 'hfn1' ...
    'hfn2' 'hfn3' 'hfn4' 'hfn5' 'hfn6' 'hpPorFonte' 'hpPorCarga' ...
    'hpPilFonte' 'hpPilCarga' 'hestlat' 'hestlon' 'preco'};
camposadd = {'estrutura' 'tensao'};

nomeArquivo = 'original.xlsx';
[dados,~,~] = xlsread(nomeArquivo,'Estrut. Primárias', 'G1:AR90');   

linhaAssoc = {5 7 9 11 13 15 43 43 45 47 17 20 23 26 35 38 29 32 49 49 ...
    49 59 62 65 68 71 76 79 82 85 88 54 17 20 23 26};

tabela = {};
for iEstr = 1:length(Estrutura)      
    
    linhaEst = linhaAssoc{iEstr};

    ehMono = false;
    for contador = 1:length(Mono)
        if isequal(Estrutura{iEstr},Mono{contador})
            ehMono = true;
        end
    end
    
    %de accordo com a tensao basta aumentar a linha
    for iTensao = 1:length(tensao)
        if ~and(ehMono,isequal(iTensao,3))
 
            linha = linhaEst - 1 + iTensao;
               
            if isempty(tabela)
                linhaTAB = 1;
            else                
                linhaTAB = length(tabela(:,1))+1;
            end
            
            tabela{linhaTAB,1} = Estrutura{iEstr};            
            tabela{linhaTAB,2} = tensao{iTensao};
            tabela{linhaTAB,3} = dados(linha,4);
            tabela{linhaTAB,4} = dados(linha,3);
            tabela{linhaTAB,5} = dados(linha,2);
            tabela{linhaTAB,6} = dados(linha,1);
            tabela{linhaTAB,7} = dados(linha,5);
            tabela{linhaTAB,8} = dados(linha,6);
            tabela{linhaTAB,9} = dados(linha,7);
            tabela{linhaTAB,10} = dados(linha,8);
            tabela{linhaTAB,11} = dados(linha,9);
            tabela{linhaTAB,12} = dados(linha,10);
            tabela{linhaTAB,13} = dados(linha,13);
            tabela{linhaTAB,14} = dados(linha,14);
            tabela{linhaTAB,15} = dados(linha,15);
            tabela{linhaTAB,16} = dados(linha,16);
            tabela{linhaTAB,17} = dados(linha,23);
            tabela{linhaTAB,18} = dados(linha,22);
            tabela{linhaTAB,19} = dados(linha,38);
            
        end
    end
end

Tcabos = cell2table(tabela,'VariableNames',{camposadd{:} campos{:}});
writetable(Tcabos,'TABELA_Estr.csv');%,'Delimiter',';');







