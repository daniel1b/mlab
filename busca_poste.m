function parametros = busca_poste(tam,res,mat,tipo)%tamanho resistencia material tipo

nomeArquivo = 'TABELA_postes.csv';
Arq = fopen(nomeArquivo);
leitura = textscan(Arq,'%s',16, 'delimiter', ',');
cabecalho = leitura{:};
poste = cell(74,4);
for i = 1:74
    leitura = textscan(Arq,'%s',1, 'delimiter', ',');
    poste(i,1) = leitura{1};
    leitura = textscan(Arq,'%s',1, 'delimiter', ',');
    poste(i,2) = leitura{1};
    leitura = textscan(Arq,'%s',1, 'delimiter', ',');
    poste(i,3) = leitura{1};
    leitura = textscan(Arq,'%s',1, 'delimiter', ',');%descarta uma leitura
    leitura = textscan(Arq,'%s',1, 'delimiter', ',');
    poste(i,4) = leitura{1};
    for j = 1:11
        textscan(Arq,'%s',1, 'delimiter', ',');
    end
end
fclose(Arq);



linha = -1;
for i = 1:length(poste(:,1))
    if and(isequal(poste{i,1}, mat),isequal(poste{i,2}, tam))
        if and(isequal(poste{i,3}, tipo),isequal(poste{i,4}, res))
            linha = i;
            break;
        end
    end
end
linha+1

if(linha<0)
    parametros = 'cobinacao nao disponivel';
else  
    dados = csvread(nomeArquivo, 1,3,[1 3 74 16]);
    %dados contem a linha do csv correnspondente a estrutura e a tensao
    dados = dados(linha,:);
    for i = 1:length(cabecalho)-3
        eval(strcat('parametros.',cabecalho{i+3},' = ',num2str(dados(i)),';'));
    end
end

end