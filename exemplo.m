clc; clear; clf;

Poste.Hposte = 14;
Poste.Posicao = 'normal';


AT(1).tensao = '13.8kV';
AT(1).estrutura = 'N1';
AT(1).pino = 'pilar';

AT(2).tensao = '13.8kV';
AT(2).estrutura = 'P1';
AT(2).pino = 'pilar';

AT(3).tensao = '13.8kV';
AT(3).estrutura = 'UD-U1';
AT(3).pino = 'porcelana';

Baixa.estrutura = 'A4';
Trafo = 1;

Montagem.Poste = Poste;
Montagem.AT = AT;
Montagem.Baixa = Baixa;
Montagem.Trafo = Trafo;

ponto = realizaMontagem(Montagem);
desenho(ponto);
text(-10,16,strcat('R$ ',int2str(ponto.preco),'. Nao incluso poste e trafo.' ));


%caracteristicasposte tam,res,mat,tipo
%Montagem.resistencia = '300';
%Montagem.material = 'Concreto';
%Montagem.tipo = 'Duplo T';
%caracteristicas nivel 1
