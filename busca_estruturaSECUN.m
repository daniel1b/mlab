function parametros = busca_estruturaSECUN(estrBuscar)

    nomeArquivo = 'TABELA_EstrBAIXA.csv';  
    Arq = fopen(nomeArquivo);
    leitura = textscan(Arq,'%s',12, 'delimiter', ',');
    cabecalho = leitura{:};
    estrutura = cell(10,1);
    for i = 1:10
        leitura = textscan(Arq,'%s',1, 'delimiter', ',');
        estrutura(i,1) = leitura{1};
        for j = 1:11
            textscan(Arq,'%s',1, 'delimiter', ','); 
        end
    end
    fclose(Arq);
    
    linha = -1;
    for i = 1:length(estrutura(:,1))
        if isequal(estrutura{i,1}, estrBuscar)
            linha = i;                    
        end
    end

    if(linha<0)
        parametros = 'cobinacao nao disponivel';
    else        

        dados = csvread(nomeArquivo, linha,1,[linha 1 linha 11]);
        %dados contem a linha do csv correnspondente a estrutura e a tensao

        for i = 1:length(cabecalho)-1
            eval(strcat('parametros.',cabecalho{i+1},' = ',num2str(dados(i)),';'));    
        end
    end
end