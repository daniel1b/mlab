function saida = calcula_Secundario(estrutura,Poste)
    DimFixas = busca_estruturaSECUN(estrutura);    
    saida.estrutura = estrutura;
    saida.preco = DimFixas.preco;
    if isequal(Poste.posicao,'normal')
        saida.hn = DimFixas.hn_normal;
        saida.ha = DimFixas.hfa_normal;
        saida.hb = DimFixas.hfb_normal;
        saida.hc = DimFixas.hfc_normal;
        saida.hd = DimFixas.hfd_normal;        
    else
        saida.hn = DimFixas.hn_topo;
        saida.ha = DimFixas.hfa_topo;
        saida.hb = DimFixas.hfb_topo;
        saida.hc = DimFixas.hfc_topo;
        saida.hd = DimFixas.hfd_topo;
    end
    
    if Poste.neutro<saida.hn
        saida = NaN;
    end
end