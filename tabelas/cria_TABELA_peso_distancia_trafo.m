function cria_TABELA_peso_distancia_trafo()
%% cria tabela para estruturas

campos = {'tensao' 'potencia' 'peso' 'distancia_cm'};

%%
nomeArq = 'C:\Users\Leonardo\Documents\PeD_318\Codigos\tabelas\Tabelas.xlsx';
dados = xlsread(nomeArq,'Pesos Transformadores','C4:E31');

ii = [1 5;6 14; 15 19; 20 28];
tensao = {'13.8kV/Monof' '13.8kV/Trif' '34.5kV/Monof' '34.5kV/Trif'};

for i = 1:numel(tensao)
    for j = ii(i,1):ii(i,2)
        D{j,1} = tensao{i};
        D{j,2} = dados(j,1);
        D{j,3} = dados(j,2);
        D{j,4} = dados(j,3);
    end
end
%%
T = cell2table(D,'VariableNames',campos);
writetable(T,'TABELA_Trafo_peso_distancia.csv');

end