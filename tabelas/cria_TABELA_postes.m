function cria_TABELA_postes()

campos = {'material' 'comprimento' 'tipo' 'carga_normal' 'carga_topo' 'massa' ...
    'topo_normal' 'topo_topo' 'base_normal' 'base_topo' 'carga_rupt_placa' ...
    'placa_altura' 'placa_espessura' 'placa_comprimento' ...
    'esforco_adicional' 'preco'};

ic = {[1 4 8] [9 13 16] [17 22 26] [27 32 35] [36 41 42] [43 48 50] ...
    [51 56 58] [59 63 65] [66 70 72]};
im = 73;

nomeArq = 'C:\Users\Leonardo\Documents\PeD_318\Codigos\tabelas\Tabelas.xlsx';
[~,~,c] = xlsread(nomeArq,'Postes','D5:P77');
k = 0;
for i = 1:numel(ic)
    for j = ic{i}(1):ic{i}(2)-1
        k = k+1;
        D{k,1} = 'Concreto';
        D{k,2} = 8+i;
        D{k,3} ='Circular';
        for p = 1:13
            D{k,p+3} = dados(k,p);
        end
    end
    for j = ic{i}(2):ic{i}(3)
        k = k+1;
        D{k,1} = 'Concreto';
        D{k,2} = 8+i;
        D{k,3} ='Duplo T';
        for p = 1:13
            D{k,p+3} = dados(k,p);
        end
    end
end
for alt = 9:11
    k = k+1;
    D{k,1} = 'Madeira';
    D{k,2} = alt;
    D{k,3} ='-';
    for p = 1:13
        D{k,p+3} = dados(end,p);
    end
end
T = cell2table(D,'VariableNames',campos);
writetable(T,'TABELA_Postes.csv');
end