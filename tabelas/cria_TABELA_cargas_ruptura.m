function cria_TABELA_cargas_ruptura()
%% cria tabela para estruturas
nomeArq = 'C:\Users\Leonardo\Documents\PeD_318\Codigos\tabelas\Tabelas.xlsx';

%% estai
campos = {'tipo_estai' 'descricao' 'diametro' 'carga_ruptura'};
dados = xlsread(nomeArq,'Cargas de Ruptura','C3:C4');
D{1,1} = 'EstCord7';
D{1,2} = 'Estai de Cordoalha de A�o 7 fios';
D{1,3} = dados(1); D{1,4} = dados(2);
T = cell2table(D,'VariableNames',campos);
writetable(T,'TABELA_Estai_ruptura.csv');
%% isoladores
clear D;
campos = {'tipo_isolador' 'descricao' 'carga_ruptura'};
[~,~,dados] = xlsread(nomeArq,'Cargas de Ruptura','E3:F9');
for i = 1:size(dados,1)
    D{i,1} = ['ISOL' num2str(i)];
    D{i,2} = dados{i,1};
    D{i,3} = dados(i,2); 
end
T = cell2table(D,'VariableNames',campos);
writetable(T,'TABELA_Isolador_ruptura.csv');
%% m�o francesa plana
clear D;
campos = {'tipo' 'trac_rupt_min' 'trac_rupt_nomi' 'compr_rupt_min' 'compr_rupt_max'};
dados = xlsread(nomeArq,'Cargas de Ruptura','H5:I5');
D{1,1} = 'Plana';
D{1,2} = dados(1); D{1,3} = dados(2);
D{1,4} = NaN; D{1,5} = NaN;
%% m�o francesa perfilada
dados = xlsread(nomeArq,'Cargas de Ruptura','J5:M5');
D{2,1} = 'Perfilada';
D{2,2} = dados(1); D{2,3} = dados(2);
D{2,4} = dados(3); D{2,5} = dados(4);
T = cell2table(D,'VariableNames',campos);
writetable(T,'TABELA_Mao_Francesa_ruptura.csv');
%% cruzetas
clear D;
campos = {'tipo' 'resist_nominal' 'resist_rupt_min' 'resist_rupt_excep'};
dados = xlsread(nomeArq,'Cargas de Ruptura','B16:E18');
for i = 1:size(dados,1)
    D{i,1} = dados(i,1);
    D{i,2} = dados(i,2);
    D{i,3} = dados(i,3); 
    D{i,4} = dados(i,4); 
end
T = cell2table(D,'VariableNames',campos);
writetable(T,'TABELA_Cruzetas_ruptura.csv');
%% solo
clear D;
campos = {'tipo' 'descricao' 'coef_compress'};
[~,~,dados] = xlsread(nomeArq,'Cargas de Ruptura','O3:P7');
for i = 1:size(dados,1)
    D{i,1} = ['SOLO' num2str(i)];
    D{i,2} = dados{i,1};
    D{i,3} = dados(i,2); 
end
T = cell2table(D,'VariableNames',campos);
writetable(T,'TABELA_Solo_compressibilidade.csv');

end 
