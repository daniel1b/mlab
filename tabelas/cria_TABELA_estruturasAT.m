function cria_TABELA_estruturas()
%% cria tabela para estruturas

altura = 9:17;
tensao = {'380V' '13.8kV' '34.5kV' '34.5kV/Neutro'};
posicao = {'Normal' 'Topo'};

nomeEstrut = {'U' 'N' 'A'};
estrutU = {'U1' 'U2' 'U3' 'U4' 'U3-U3' 'U2-3' 'UD(U1/U2)' 'UD(U4)' 'UD(Trif)'};
estrutN = {'N1' 'N2' 'N3' 'N4' 'N3-N3' 'N2-3' 'ND'};
estrutA = {'A1' 'A2' 'A3' 'A4' 'A5' 'A1D' 'A2D' 'A3D' 'A4D' 'A5D'};

campos = {'hn' 'hefF' 'hefC' 'helon' 'helat' 'hnBT' 'hfaBT' 'hfbBT' 'hfcBT' 'hfdBT' 'preco'};
camposadd = {'tensao' 'estrutura' 'altura' 'posicao'};

%% 
i = 0;
for ii = 1:numel(nomeEstrut)
    estrut = eval(['estrut' nomeEstrut{ii}]);
    for j = 1:numel(tensao)
        for k = 1:numel(estrut)
            for p = 1:numel(altura)
                for q = 1:numel(posicao)
                    ident.estrut = estrut{k};
                    ident.altura = altura(p);
                    ident.tensao = tensao{j};
                    ident.posicao = posicao{q};
                    estr = busca_dados_estrutura_TABELA(ident);
                    if ~isempty(estr)
                        i = i+1;
                        D{i,1} = ident.tensao;
                        D{i,2} = ident.estrut;
                        D{i,3} = ident.altura;
                        D{i,4} = ident.posicao;
                        for r = 1:numel(campos)
                            D{i,4+r} = eval(['estr.' campos{r}]);
                        end
                    end
                end
            end
        end
    end
end
%%
Testrut = cell2table(D,'VariableNames',{camposadd{:} campos{:}});
writetable(Testrut,'TABELA_Estruturas.csv');

end