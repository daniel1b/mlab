function cria_TABELA_cruzetas()
%% cria tabela para cruzetas
%% N
campos = {'L1' 'L2' 'L3' 'L4' 'L5' 'L6' 'L7'};
camposadd = {'Tipo' 'Dimensao'};
D{1,1} = 'N';
D{1,2} = 2400;
L = [400 1100 1100 1050 1050 450 500];
for i = 1:numel(L), D{1,2+i} = L(i); end
D{2,1} = 'N';
D{2,2} = 3300;
L = [400 1550 1550 1500 1500 450 500];
for i = 1:numel(L), D{2,2+i} = L(i); end
Tcruz = cell2table(D,'VariableNames',{camposadd{:} campos{:}});
writetable(Tcruz,'TABELA_CruzetaN.csv');
%% M
clear D;
campos = {'M1' 'M2' 'M3' 'M4' 'M5' 'M6' 'M7' 'M8'};
camposadd = {'Tipo' 'Dimensao'};
D{1,1} = 'M';
D{1,2} = 2400;
M = [500 1000 1500 550 1050 1550 600 500];
for i = 1:numel(M), D{1,2+i} = M(i); end
Tcruz = cell2table(D,'VariableNames',{camposadd{:} campos{:}});
writetable(Tcruz,'TABELA_CruzetaM.csv');
%% B
clear D;
campos = {'W1' 'W2' 'W3' 'W4' 'W5' 'W6' 'W7'};
camposadd = {'Tipo' 'Dimensao'};
D{1,1} = 'B';
D{1,2} = 2400;
B = [750 1450 2150 700 1400 2100 1200];
for i = 1:numel(B), D{1,2+i} = B(i); end
Tcruz = cell2table(D,'VariableNames',{camposadd{:} campos{:}});
writetable(Tcruz,'TABELA_CruzetaB.csv');

end