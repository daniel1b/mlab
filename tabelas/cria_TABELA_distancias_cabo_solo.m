function cria_TABELA_distancias_cabo_solo()
%% cria tabela para estruturas

campos = {'codigo' 'descricao' 'comunic_cabosaterr' 'bt' 'at13' 'at34'};

%%
nomeArq = 'C:\Users\Leonardo\Documents\PeD_318\Codigos\tabelas\Tabelas.xlsx';
[~,~,dados] = xlsread(nomeArq,'Dist. Min. Cabo-solo','A3:F10');

for i = 1:size(dados,1)
    for j = 1:6
        D{i,j} = dados{i,j};
    end        
end
%%
T = cell2table(D,'VariableNames',campos);
writetable(T,'TABELA_distancias_cabo_solo.csv');

end