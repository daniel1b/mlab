function cria_TABELA_temp_vento()
nomeArq = 'C:\Users\Leonardo\Documents\PeD_318\Codigos\tabelas\Tabelas.xlsx';

campos = {'tipo_isolador' 'descricao' 'carga_ruptura'};
[~,~,dados] = xlsread(nomeArq,'Cargas de Ruptura','E3:F9');
for i = 1:size(dados,1)
    D{i,1} = ['ISOL' num2str(i)];
    D{i,2} = dados{i,1};
    D{i,3} = dados(i,2); 
end
T = cell2table(D,'VariableNames',campos);
writetable(T,'TABELA_Isolador_ruptura.csv');

end 
