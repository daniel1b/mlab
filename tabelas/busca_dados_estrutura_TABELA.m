function estr = busca_dados_estrutura_TABELA(ident)
%% alterar o caminho para o arquivo
nomeArq = 'C:\Users\Leonardo\Documents\PeD_318\Codigos\tabelas\Tabelas.xlsx';
xlswrite(nomeArq,ident.altura,1,'D5'); % verificar consist�ncia da altura do poste
%% U
switch ident.estrut
    case 'U1'
        estr = busca_dados_U(ident,nomeArq,'N5:AK5','N6:AK6');
    case 'U2'
        estr = busca_dados_U(ident,nomeArq,'N7:AK7','N8:AK8');
    case 'U3'
        estr = busca_dados_U(ident,nomeArq,'N9:AK9','N10:AK10');
    case 'U4'
        estr = busca_dados_U(ident,nomeArq,'N11:AK11','N12:AK12');
    case 'U3-U3'
        estr = busca_dados_U(ident,nomeArq,'N13:AK13','N14:AK14');
    case 'U2-3'
        estr = busca_dados_U(ident,nomeArq,'N15:AK15','N16:AK16');
    case 'UD(U1/U2)'
        estr = busca_dados_U(ident,nomeArq,'N43:AK43','N44:AK44');
    case 'UD(U4)'
        estr = busca_dados_U(ident,nomeArq,'N45:AK45','N46:AK46');
    case 'UD(Trif)'
        estr = busca_dados_U(ident,nomeArq,'N47:AK47','N48:AK48');
    otherwise
end
%% N
switch ident.estrut
    case 'N1'
        estr = busca_dados_N(ident,nomeArq,'N17:AJK7','N18:AK18','N19:AK19');
    case 'N2'
        estr = busca_dados_N(ident,nomeArq,'N20:AK20','N21:AK21','N22:AK22');
    case 'N3'
        estr = busca_dados_N(ident,nomeArq,'N23:AK23','N24:AK24','N25:AK25');
    case 'N4'
        estr = busca_dados_N(ident,nomeArq,'N26:AK26','N27:AK27','N28:AK28');
    case 'N3-N3'
        estr = busca_dados_N(ident,nomeArq,'N35:AK35','N36:AK36','N37:AK37');
    case 'N2-3'
        estr = busca_dados_N(ident,nomeArq,'N38:AK38','N39:AK39','N40:AK40');
    case 'ND'
        estr = busca_dados_N(ident,nomeArq,'N49:AK49','N50:AK50','N51:AK51');
    otherwise
end
%% A
switch ident.estrut
    case 'A1'
        estr = busca_dados_A(ident,nomeArq,'D5:P5');
    case 'A2'
        estr = busca_dados_A(ident,nomeArq,'D6:P6');
    case 'A3'
        estr = busca_dados_A(ident,nomeArq,'D7:P7');
    case 'A4'
        estr = busca_dados_A(ident,nomeArq,'D8:P8');
    case 'A5'
        estr = busca_dados_A(ident,nomeArq,'D9:P9');
    case 'A1D'
        estr = busca_dados_A(ident,nomeArq,'D10:P10');
    case 'A2D'
        estr = busca_dados_A(ident,nomeArq,'D11:P11');
    case 'A3D'
        estr = busca_dados_A(ident,nomeArq,'D12:P12');
    case 'A4D'
        estr = busca_dados_A(ident,nomeArq,'D13:P13');
    case 'A5D'
        estr = busca_dados_A(ident,nomeArq,'D14:P14');
    otherwise
end

%% demais tipos de estruturas ...
end

%% U
function estr = busca_dados_U(ident,nomeArq,int1,int2)
estr = [];
switch ident.tensao
    case '13.8kV'
        dados = xlsread(nomeArq,1,int1); % strigs = NaN
    case '34.5kV'
        dados = xlsread(nomeArq,1,int2);
    otherwise
        disp('Tens�o indefinida!'); return;
end
if isequal(ident.posicao,'Normal')
    estr.hn = dados(1);
    estr.hefF = dados(4); estr.hefC = dados(5);
    estr.helon = dados(10); estr.helat = dados(12);
    if dados(14) < estr.hn
        estr.hnBT = dados(14); estr.hfaBT = dados(16);
        estr.hfbBT = dados(18); estr.hfcBT = dados(20); estr.hfdBT = dados(22);
    else
        estr.hnBT = NaN; estr.hfaBT = NaN;
        estr.hfbBT = NaN; estr.hfcBT = NaN; estr.hfdBT = NaN;
    end
elseif isequal(ident.posicao,'Topo')
    estr.hn = dados(2);
    estr.hefF = dados(6); estr.hefC = dados(7);
    estr.helon = dados(11); estr.helat = dados(13);
    if dados(15) < estr.hn
        estr.hnBT = dados(15); estr.hfaBT = dados(17);
        estr.hfbBT = dados(19); estr.hfcBT = dados(21); estr.hfdBT = dados(23);
    else
        estr.hnBT = NaN; estr.hfaBT = NaN;
        estr.hfbBT = NaN; estr.hfcBT = NaN; estr.hfdBT = NaN;
    end
else
    disp('Nem "Normal", nem "Topo"!'); return;
end
estr.preco = dados(end);
% verticais
end

%% N
function estr = busca_dados_N(ident,nomeArq,int1,int2,int3)
estr = [];
switch ident.tensao
    case '13.8kV'
        dados = xlsread(nomeArq,1,int1); % strigs = NaN
    case '34.5kV'
        dados = xlsread(nomeArq,1,int2);
    case '34.5kV/Neutro'
        dados = xlsread(nomeArq,1,int3);
    otherwise
        disp('Tens�o indefinida!'); return;
end
%% horizontais
if isequal(ident.posicao,'Normal')
    estr.hn = dados(1);
    estr.hefF = dados(4); estr.hefC = dados(5);
    estr.helon = dados(10); estr.helat = dados(12);
    if dados(14) < estr.hn
        estr.hnBT = dados(14); estr.hfaBT = dados(16);
        estr.hfbBT = dados(18); estr.hfcBT = dados(20); estr.hfdBT = dados(22);
    else
        estr.hnBT = NaN; estr.hfaBT = NaN;
        estr.hfbBT = NaN; estr.hfcBT = NaN; estr.hfdBT = NaN;
    end
elseif isequal(ident.posicao,'Topo')
    estr.hn = dados(2);
    estr.hefF = dados(6); estr.hefC = dados(7);
    estr.helon = dados(11); estr.helat = dados(13);
    if dados(15) < estr.hn
        estr.hnBT = dados(15); estr.hfaBT = dados(17);
        estr.hfbBT = dados(19); estr.hfcBT = dados(21); estr.hfdBT = dados(23);
    else
        estr.hnBT = NaN; estr.hfaBT = NaN;
        estr.hfbBT = NaN; estr.hfcBT = NaN; estr.hfdBT = NaN;
    end
else
    disp('Nem "Normal", nem "Topo"!'); return;
end
% % %% verticais
% % if isequal(ident.cruzeta,'2400')
% %     dados = xlsread(nomeArq,5,'D19:D25'); % aba n� 5 (Cruzetas)
% %     estr.L = dados;
% % elseif isequal(ident.cruzeta,'3300')
% %     dados = xlsread(nomeArq,5,'E19:E25'); % aba n� 5 (Cruzetas)
% %     estr.L = dados;
% % else
% %     estr.L = NaN;
% %     disp('Dimens�o de cruzeta inexistente!')
% % end
estr.preco = dados(end);
end
%% A
function estr = busca_dados_A(ident,nomeArq,int1)
estr = [];
if isequal(ident.tensao,'380V')
    dados = xlsread(nomeArq,3,int1); % strigs = NaN
    if isequal(ident.posicao,'Normal')
        estr.hn = NaN;
        estr.hefF = NaN; estr.hefC = NaN;
        estr.helon = NaN; estr.helat = NaN;
        estr.hnBT = dados(3); estr.hfaBT = dados(5);
        estr.hfbBT = dados(7); estr.hfcBT = dados(9); estr.hfdBT = dados(11);
    elseif isequal(ident.posicao,'Topo')
        estr.hn = NaN;
        estr.hefF = NaN; estr.hefC = NaN;
        estr.helon = NaN; estr.helat = NaN;
        estr.hnBT = dados(4); estr.hfaBT = dados(6);
        estr.hfbBT = dados(8); estr.hfcBT = dados(10); estr.hfdBT = dados(12);
    else
        disp('Nem "Normal", nem "Topo"!'); return;
    end
    estr.preco = dados(end);
end
% verticais
end
