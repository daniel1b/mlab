function cria_TABELA_estruturas_com_trafo()
%% cria tabela para estruturas

altura = 9:17;
tensao = {'13.8kV' '34.5kV/Neutro'};
posicao = {'Normal' 'Topo'};

nomeEstrut = {'U' 'N'};
estrutU = {'U3'};
estrutN = {'N3'};

campos = {'htrf' 'hn' 'hefF' 'hefC' 'helon' 'helat' 'hnBT' 'hfaBT' 'hfbBT' 'hfcBT' 'hfdBT' 'preco'};
camposadd = {'tensao' 'estrutura' 'altura' 'posicao'};

%%
i = 0;
for ii = 1:numel(nomeEstrut)
    estrut = eval(['estrut' nomeEstrut{ii}]);
    for j = 1:numel(tensao)
        for k = 1:numel(estrut)
            for p = 1:numel(altura)
                for q = 1:numel(posicao)
                    ident.estrut = estrut{k};
                    ident.altura = altura(p);
                    ident.tensao = tensao{j};
                    ident.posicao = posicao{q};
                    estr = busca_dados_estrutura_TABELA(ident);
                    if ~isempty(estr)
                        nomeArq = 'C:\Users\Leonardo\Documents\PeD_318\Codigos\tabelas\Tabelas.xlsx';
                        if isequal(ident.tensao,'13.8kV')
                            cel = 'N7';
                            if isequal(ident.posicao,'Normal'), cel = 'M7'; end
                        else
                            cel = 'N8';
                            if isequal(ident.posicao,'Normal'), cel = 'M8'; end
                        end
                        estr.htrf = xlsread(nomeArq,'Estrut. Trafo',cel);
                        if isempty(estr.htrf), estr.htrf = NaN; end
                        i = i+1;
                        D{i,1} = ident.tensao;
                        D{i,2} = ident.estrut;
                        D{i,3} = ident.altura;
                        D{i,4} = ident.posicao;
                        for r = 1:numel(campos)
                            D{i,4+r} = eval(['estr.' campos{r}]);
                        end
                    end
                end
            end
        end
    end
end
%%
Testrut = cell2table(D,'VariableNames',{camposadd{:} campos{:}});
writetable(Testrut,'TABELA_Estruturas_com_Trafo.csv');

end