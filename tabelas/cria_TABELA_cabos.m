%function cria_TABELA_cabos()
%% cria tabela para cabos
tipo = {'CA' 'CAA' 'MULTIPLEX'};
secaoCA = {'4' '2' '1/0' '2/0' '4/0' '266.8' '336.4'};
secaoCAA = {'4' '2' '1/0' '2/0' '4/0' '266.8' '336.4' '397.5' '636' ...
    '795' '605' '954' '101.8' '110.8' '134.6'};
secaoMULTIPLEX = {'1x1x25 + 25mm2'  '1x1x35 + 35mm2' '1x1x50 + 50mm2' ...
    '2x1x25 + 25mm2' '2x1x35 + 35mm2' '2x1x50 + 50mm2' '2x1x70 + 70mm2' ...
    '3x1x25 + 25mm2' '3x1x35 + 35mm2' '3x1x50 + 50mm2' '3x1x70 + 70mm2' ...
    '3x1x95 + 70mm2' '3x1x120 + 70mm2'};

campos = {'peso' 'tensao_ruptura' 'area' 'coef_dilat_ini' 'coef_dilat_fin' ...
    'elast_ini_inf' 'elast_ini_sup' 'elast_fin' 'vao_tipico' ...
    'cap_conducao' 'resistencia' 'preco'};
camposadd = {'Cabo' 'Secao'};

%% CA
i = 0;
for j = 1
    for k = 1:numel(secaoCA)
        ident.tipo = tipo{j};
        ident.secao = secaoCA{k};
        cabo = busca_dados_cabo_TABELA(ident);
        i = i+1;
        D{i,1} = ident.tipo;
        D{i,2} = ident.secao;
        for r = 1:numel(campos)
            D{i,2+r} = eval(['cabo.' campos{r}]);
        end
    end
end

%% CAA
for j = 2
    for k = 1:numel(secaoCAA)
        ident.tipo = tipo{j};
        ident.secao = secaoCAA{k};
        cabo = busca_dados_cabo_TABELA(ident);
        i = i+1;
        D{i,1} = ident.tipo;
        D{i,2} = ident.secao;
        for r = 1:numel(campos)
            D{i,2+r} = eval(['cabo.' campos{r}]);
        end
    end
end
%% MULTIPLEX
for j = 3
    for k = 1:numel(secaoMULTIPLEX)
        ident.tipo = tipo{j};
        ident.secao = secaoMULTIPLEX{k};
        cabo = busca_dados_cabo_TABELA(ident);
        i = i+1;
        D{i,1} = ident.tipo;
        D{i,2} = ident.secao;
        for r = 1:numel(campos)
            D{i,2+r} = eval(['cabo.' campos{r}]);
        end
    end
end

Tcabos = cell2table(D,'VariableNames',{camposadd{:} campos{:}});
writetable(Tcabos,'TABELA_Cabos.csv');

%end