function cabo = busca_dados_cabo_TABELA(ident)
nomeArq = 'C:\Users\Daniel\Desktop\mlab\Original.xlsx';
cabo = [];
if isequal(ident.tipo,'CA')
    %%
    dados = xlsread(nomeArq,'Cabos','E4:AE10');
    switch ident.secao
        case '4', i = 1;
        case '2', i = 2;
        case '1/0', i = 3;
        case '2/0', i = 4;
        case '4/0', i = 5;
        case '266.8', i = 6;
        case '336.4', i = 7;
        otherwise, disp('Se��o inexistente!'); return;
    end
    cabo.peso = dados(i,16); 
    cabo.tensao_ruptura = dados(i,17); 
    cabo.area = dados(i,2); 
    cabo.coef_dilat_ini = dados(i,21);
    cabo.coef_dilat_fin = dados(i,22);
    cabo.elast_ini_inf = dados(i,23);
    cabo.elast_ini_sup = dados(i,24);
    cabo.elast_fin = dados(i,25);
    cabo.vao_tipico = dados(i,20);
    cabo.cap_conducao = dados(i,19);
    cabo.resistencia = dados(i,18);
    cabo.preco = dados(i,26);
    
elseif isequal(ident.tipo,'CAA')
    %%
    dados = xlsread(nomeArq,'Cabos','E11:AE25');
    switch ident.secao
        case '4', i = 1;
        case '2', i = 2;
        case '1/0', i = 3;
        case '2/0', i = 4;
        case '4/0', i = 5;
        case '266.8', i = 6;
        case '336.4', i = 7;
        case '397.5', i = 8;
        case '636', i = 9;
        case '795', i = 10;
        case '605', i = 11;
        case '954', i = 12;
        case '101.8', i = 13;
        case '110.8', i = 14;
        case '134.6', i = 15;
        otherwise, disp('Se��o inexistente!'); return;
    end
    cabo.peso = dados(i,16); 
    cabo.tensao_ruptura = dados(i,17); 
    cabo.area = dados(i,2); 
    cabo.coef_dilat_ini = dados(i,21);
    cabo.coef_dilat_fin = dados(i,22);
    cabo.elast_ini_inf = dados(i,23);
    cabo.elast_ini_sup = dados(i,24);
    cabo.elast_fin = dados(i,25);
    cabo.vao_tipico = dados(i,20);
    cabo.cap_conducao = dados(i,19);
    cabo.resistencia = dados(i,18);
    cabo.preco = dados(i,26);
    
elseif isequal(ident.tipo,'MULTIPLEX')
    %%
    dados = xlsread(nomeArq,'Cabos','E28:J40');
    switch ident.secao
        case '1x1x25 + 25mm2', i = 1;
        case '1x1x35 + 35mm2', i = 2;
        case '1x1x50 + 50mm2', i = 3;
        case '2x1x25 + 25mm2', i = 4;
        case '2x1x35 + 35mm2', i = 5;
        case '2x1x50 + 50mm2', i = 6;
        case '2x1x70 + 70mm2', i = 7;
        case '3x1x25 + 25mm2', i = 8;
        case '3x1x35 + 35mm2', i = 9;
        case '3x1x50 + 50mm2', i = 10;
        case '3x1x70 + 70mm2', i = 11;
        case '3x1x95 + 70mm2', i = 12;
        case '3x1x120 + 70mm2', i = 13;
        otherwise, disp('Se��o inexistente!'); return;
    end
    cabo.peso = dados(i,1);
    cabo.tensao_ruptura = dados(i,3);
    cabo.area = NaN; 
    cabo.coef_dilatacao = NaN;
    cabo.elasticidade = NaN;
    cabo.vao_tipico = dados(i,5);
    cabo.cap_conducao = NaN;
    cabo.resistencia = NaN;

    cabo.peso = dados(i,1); 
    cabo.tensao_ruptura = dados(i,3); 
    cabo.area = NaN; 
    cabo.coef_dilat_ini = NaN;
    cabo.coef_dilat_fin = NaN;
    cabo.elast_ini_inf = NaN;
    cabo.elast_ini_sup = NaN;
    cabo.elast_fin = NaN;
    cabo.vao_tipico = dados(i,5);
    cabo.cap_conducao = NaN;
    cabo.resistencia = NaN;
    cabo.preco = NaN;
    
else
    disp('Tipo inexistente de cabo!'); return;
end

cabo.ident = ident;

end
