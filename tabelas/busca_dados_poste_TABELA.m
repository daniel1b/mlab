function poste = busca_dados_poste_TABELA(ident)
nomeArq = 'C:\Users\Leonardo\Documents\PeD_318\Codigos\tabelas\Estruturas.xlsx';
dados = xlsread(nomeArq,7,'D5:O53');
poste = [];
%%
switch ident.altura
    case 9
        if isequal(ident.forma,'Circular'), i = 1; j = 3;
        elseif isequal(ident.forma,'Duplo T'), i = 4; j = 8;
        else disp('Tipo de poste inexistente!');
        end
    case 10
        if isequal(ident.forma,'Circular'), i = 9; j = 12;
        elseif isequal(ident.forma,'Duplo T'), i = 13; j = 16;
        else disp('Tipo de poste inexistente!');
        end
    case 11
        if isequal(ident.forma,'Circular'), i = 17; j = 21;
        elseif isequal(ident.forma,'Duplo T'), i = 22; j = 26;
        else disp('Tipo de poste inexistente!');
        end
    case 12
        if isequal(ident.forma,'Circular'), i = 27; j = 31;
        elseif isequal(ident.forma,'Duplo T'), i = 32; j = 35;
        else disp('Tipo de poste inexistente!');
        end
    case 13
        if isequal(ident.forma,'Circular'), i = 36; j = 40;
        elseif isequal(ident.forma,'Duplo T'), i = 41; j = 42;
        else disp('Tipo de poste inexistente!');
        end
    case 14
        if isequal(ident.forma,'Circular'), i = 43; j = 46;
        elseif isequal(ident.forma,'Duplo T'), i = 47; j = 49;
        else disp('Tipo de poste inexistente!');
        end
    case 15 % falta!
        if isequal(ident.forma,'Circular'), i = 17; j = 21;
        elseif isequal(ident.forma,'Duplo T'), i = 22; j = 26;
        else disp('Tipo de poste inexistente!');
        end
    case 16 % falta!
        if isequal(ident.forma,'Circular'), i = 17; j = 21;
        elseif isequal(ident.forma,'Duplo T'), i = 22; j = 26;
        else disp('Tipo de poste inexistente!');
        end
    case 17 % falta!
        if isequal(ident.forma,'Circular'), i = 17; j = 21;
        elseif isequal(ident.forma,'Duplo T'), i = 22; j = 26;
        else disp('Tipo de poste inexistente!');
        end
    case 18 % falta!
        if isequal(ident.forma,'Circular'), i = 17; j = 21;
        elseif isequal(ident.forma,'Duplo T'), i = 22; j = 26;
        else disp('Tipo de poste inexistente!');
        end
    case 19 % falta!
        if isequal(ident.forma,'Circular'), i = 17; j = 21;
        elseif isequal(ident.forma,'Duplo T'), i = 22; j = 26;
        else disp('Tipo de poste inexistente!');
        end
    case 20 % falta!
        if isequal(ident.forma,'Circular'), i = 17; j = 21;
        elseif isequal(ident.forma,'Duplo T'), i = 22; j = 26;
        else disp('Tipo de poste inexistente!');
        end
    otherwise
        disp('Altura de poste inexistente!'); return;
end
%%
poste.ident = ident;
switch ident.tipo
    case 'Normal'
        poste.carga_nominal = dados(i:j,1);
        poste.massa = dados(i:j,3);
        poste.dimensao_topo = dados(i:j,4);
        poste.dimensao_base = dados(i:j,6);
        poste.carga_ruptura_placa = dados(i:j,8);
        poste.altura_placa = dados(i:j,9);
        poste.espessura_placa = dados(i:j,10);
        poste.comprimento_placa = dados(i:j,11);
        poste.resistencia_adicional_engastamento = dados(i:j,12);
    case 'Topo'
        poste.carga_nominal = dados(i:j,2);
        poste.massa = dados(i:j,3);
        poste.dimensao_topo = dados(i:j,5);
        poste.dimensao_base = dados(i:j,7);
        poste.carga_ruptura_placa = dados(i:j,8);
        poste.altura_placa = dados(i:j,9);
        poste.espessura_placa = dados(i:j,10);
        poste.comprimento_placa = dados(i:j,11);
        poste.resistencia_adicional_engastamento = dados(i:j,12);
    otherwise
        disp('Tipo de poste inexistente!');
end
    
end

