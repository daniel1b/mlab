function desenho(adesenhar)
%............desenhar um chao..................
    x1 = [-10 10 10 -10];
    y1 = [0 0 -4 -4];
    patch(x1,y1,[0.5 0 0.25]);
%.............desenhar o trafo....................
    if ~isnan(adesenhar.Trafo)
        ht = adesenhar.Trafo;
        x1 = [-1 -1 1 1];
        y1 = [(ht-0.7) (ht+0.7) (ht+0.7) (ht-0.7)];
        patch(x1,y1,[0.3 0.3 0.3]);
        
        x1 = [-0.9 -0.9 -0.5 -0.5];
        y1 = [(ht+0.7) (ht+0.9) (ht+0.9) (ht+0.7)];
        patch(x1,y1,'blue');

    end
%............desenhar o poste..................
    Hlivre = adesenhar.Poste.Hlivre;
    engast = adesenhar.Poste.engastamento;
    x1 = [-0.4 0.4 0.2 -0.2];
    y1 = [-engast -engast Hlivre Hlivre];
    patch(x1,y1,[0.6 0.6 0.6]);
%.............desenhar os Niveis..................
    for m = 1:length(adesenhar.AT) % desenhando o 1 e 2
            for i = 1:3
                hesf = eval(['adesenhar.AT(m).esforcoFase' int2str(i) 'Fonte']);
                hfase = eval(['adesenhar.AT(m).cabo' int2str(i) 'SoloFonte']);
                if ~isnan(hfase)%se existir cabo na fonte
                    %isolamento fonte:------------------------
                    x1 = [-0.3 -0.3 -0.2 -0.2 ];
                    y1 = [(hesf-0.15) hfase hfase (hesf-0.15)];
                    patch(x1,y1,[0 0 0]);
                    x1 = [-0.35 -0.35 -0.15 -0.15];
                    y1 = [(hfase-0.15) (hfase+0.15) (hfase+0.15) (hfase-0.15)];
                    patch(x1,y1,'blue');
                    %fonte:
                    x1 = [-10 -10 -0.2 -0.2];
                    y1 = [(hfase+0.02) (hfase-0.02) (hfase-0.02) (hfase+0.02)];
                    patch(x1,y1,'green');
                    p1 = hfase;
                    p2 = p1;
                end
                hesf = eval(['adesenhar.AT(m).esforcoFase' int2str(i) 'Carga']);
                hfase = eval(['adesenhar.AT(m).cabo' int2str(i) 'SoloCarga']);
                if ~isnan(hfase)
                    %isolamento Carga:----------------------
                    if ~isnan(hesf)
                        x1 = [0.3 0.3 0.2 0.2 ];
                        y1 = [(hesf-0.15) hfase hfase (hesf-0.15)];
                        patch(x1,y1,[0 0 0]);
                        x1 = [0.35 0.35 0.15 0.15];
                        y1 = [(hfase-0.15) (hfase+0.15) (hfase+0.15) (hfase-0.15)];
                        patch(x1,y1,'blue');
                    end
                    %Carga:
                    x1 = [10 10 0.2 0.2];
                    y1 = [(hfase+0.02) (hfase-0.02) (hfase-0.02) (hfase+0.02)];
                    patch(x1,y1,'green');
                    p2 = hfase;
                    x1 = [-0.2 -0.2 0.2 0.2];
                    y1 = [(p1-0.02) (p1+0.02) (p2+0.02) (p2-0.02)];
                    patch(x1,y1,'green');
                end
            end
    end
%.................desenhar o neutro......................................
    if ~isnan(adesenhar.Poste.neutro)
        hn = adesenhar.Poste.neutro;
        %roldana:
            x1 = [-.1 -.1 .1 .1];
            y1 = [(hn-0.1) (hn+0.1) (hn+0.1) (hn-0.1)];
            patch(x1,y1,[0.5 0 0.25]);
        %neutro:        
            x1 = [-10 -10 10 10];
            y1 = [(hn+0.02) (hn-0.02) (hn-0.02) (hn+0.02)];
            patch(x1,y1,'blue');
    end

%................desenhar a baixa tensao................................
    if isstruct(adesenhar.Baixa)
        fases = {'ha' 'hb' 'hc' 'hd'};
        for i = 1:4 % desenhando o 1 e 2
            if ~isnan(eval(['adesenhar.Baixa.' fases{i}]))
                hx = eval(['adesenhar.Baixa.' fases{i}]);
                %roldana:
                    x1 = [-.1 -.1 .1 .1];
                    y1 = [(hx-0.1) (hx+0.1) (hx+0.1) (hx-0.1)];
                    patch(x1,y1,[0.5 0 0.25]);
                %neutro:        
                    x1 = [-10 -10 10 10];
                    y1 = [(hx+0.02) (hx-0.02) (hx-0.02) (hx+0.02)];
                    patch(x1,y1,'green');
            end
        end
    end
    %.............desenhar a baixa tensao..............................
    if ~isnan(adesenhar.Poste.hestlon)
        he = adesenhar.Poste.hestlon;
        %cabo:
        x1 = [-0.2 -0.2 -2 -2];
        y1 = [he (he-0.04) (he-1) (he-0.96)];
        patch(x1,y1,[0 0 0]);
        x1 = [0.2 0.2 2 2];
        y1 = [he (he-0.04) (he-1) (he-0.96)];
        patch(x1,y1,[0 0 0]);
    end

    if ~isnan(adesenhar.Poste.hestlat)
        he = adesenhar.Poste.hestlat;
        %chapa:
        x1 = [-.1 -.1 .1 .1];
        y1 = [(he-0.1) (he+0.1) (he+0.1) (he-0.1)];
        patch(x1,y1,[0.4 0.4 0.4]);
        %cabo:
        x1 = [-0.02 -0.02 0.02 0.02];
        y1 = [he (he-1) (he-1) he];
        patch(x1,y1,[0 0 0]);
    end



   
    
            
            
 
    set(gca,'XTick',[-10:1:10]);
    set(gca,'YTick',[-4:1:17]);
    
%axis([-10 10 -4 17]);    
    xlim([-15 15]);
    ylim([-4 17]);
end