function saidaT = calcula_Nivel1(Poste,Nivel1,grupo)
saida = Nivel1;%pegar os dados referente a estrutura, tensao, pino e ja colocar na saida
dadosBrutos = busca_estruturaPRIM(Nivel1.estrutura,Nivel1.tensao);
saida.preco = dadosBrutos.preco;
if isequal(Poste.posicao,'normal')% usar nos calculos os valores de normal
    DimFixas.hf = dadosBrutos.hf_normal;
    DimFixas.hc = dadosBrutos.hc_normal;
else% usar nos calculos os valores de topo
    DimFixas.hf = dadosBrutos.hf_topo;
    DimFixas.hc = dadosBrutos.hc_topo;
end
if isequal(Nivel1.pino,'porcelana')% usar nos calculos os valores dos pinos de porcelana
    DimFixas.hpinoc = dadosBrutos.hpPorCarga;
    DimFixas.hpinof = dadosBrutos.hpPorFonte;
else% usar nos calculos os valores dos pinos pilar
    DimFixas.hpinoc = dadosBrutos.hpPilCarga;
    DimFixas.hpinof = dadosBrutos.hpPilFonte;
end

%----complemento d altura da metade da cruzeta-----------------
comp = 0.112/2;


DimFixas.hfn1 = dadosBrutos.hfn1;
DimFixas.hfn2 = dadosBrutos.hfn2;
DimFixas.hfn3 = dadosBrutos.hfn3;
DimFixas.hfn4 = dadosBrutos.hfn4;
DimFixas.hfn5 = dadosBrutos.hfn5;
DimFixas.hfn6 = dadosBrutos.hfn6;

DimFixas.hestlat = dadosBrutos.hestlat;
DimFixas.hestlon = dadosBrutos.hestlon;

%comecar o calculo das dimensoes:
    switch grupo
        case 1 %script para o calculo do grupo1 de estruturas++++++++++++++++++++++++++++++++++++++++
            if(~isnan(DimFixas.hc))  %descobre se tem cabos do lado da carga
                saida.esforcoFase1Fonte = Poste.Hlivre - DimFixas.hf;
                saida.neutroSolo = saida.esforcoFase1Fonte - DimFixas.hfn2;
                if(~isnan(DimFixas.hpinof))
                    saida.cabo1SoloFonte = Poste.Hlivre - DimFixas.hf + DimFixas.hpinof;
                else
                    saida.cabo1SoloFonte = Poste.Hlivre - DimFixas.hf;
                end
                if ~isnan(DimFixas.hpinoc)
                    if(DimFixas.hpinoc>0)
                        saida.esforcoFase1Carga = Poste.Hlivre - DimFixas.hc;
                        saida.cabo1SoloCarga = Poste.Hlivre - DimFixas.hc + DimFixas.hpinoc;
                    else
                        saida.esforcoFase1Carga = NaN;
                        saida.cabo1SoloCarga = saida.cabo1SoloFonte;
                    end

                else
                    saida.esforcoFase1Carga = Poste.Hlivre - DimFixas.hc;
                    saida.cabo1SoloCarga = Poste.Hlivre - DimFixas.hc;
                    saida.neutroSolo = saida.esforcoFase1Carga - DimFixas.hfn2;
                end

            else %caso so tenha cabos do lado da fonte
                saida.esforcoFase1Fonte = Poste.Hlivre - DimFixas.hf;
                saida.esforcoFase1Carga = NaN;
                saida.cabo1SoloCarga = NaN;
                if(~isnan(DimFixas.hpinof))
                    saida.cabo1SoloFonte = Poste.Hlivre - DimFixas.hf + DimFixas.hpinof;
                else
                    saida.cabo1SoloFonte = Poste.Hlivre - DimFixas.hf;
                end
                saida.neutroSolo = saida.esforcoFase1Fonte - DimFixas.hfn1;
            end

            ref = saida.esforcoFase1Fonte;

            saida.hestlat = NaN;
            saida.hestlon = NaN;

            if ~isnan(DimFixas.hestlat)
                saida.hestlat = ref - DimFixas.hestlat;
            end
            if ~isnan(DimFixas.hestlon)
                saida.hestlon = ref - DimFixas.hestlon;
            end

            saida.esforcoFase2Fonte = NaN;
            saida.esforcoFase3Fonte = NaN;
            saida.esforcoFase2Carga = NaN;
            saida.esforcoFase3Carga = NaN;
            saida.cabo2SoloCarga = NaN;
            saida.cabo3SoloCarga = NaN;
            saida.cabo2SoloFonte = NaN;
            saida.cabo3SoloFonte = NaN;

        case 2 %script para o calculo do grupo2 de estruturas+++++++++++++++++++++++++++++++++++++++
            if(~isnan(DimFixas.hc))  %descobre se tem cabos do lado da carga
                saida.esforcoFase1Fonte = Poste.Hlivre - DimFixas.hf;
                if(~isnan(DimFixas.hpinof))
                    saida.cabo1SoloFonte = Poste.Hlivre - DimFixas.hf + DimFixas.hpinof + comp;
                else
                    saida.cabo1SoloFonte = Poste.Hlivre - DimFixas.hf ;
                end
                if ~isnan(DimFixas.hpinoc)
                    if(DimFixas.hpinoc>0)
                        saida.esforcoFase1Carga = Poste.Hlivre - DimFixas.hc;
                        saida.cabo1SoloCarga = Poste.Hlivre - DimFixas.hc + DimFixas.hpinoc + comp;
                    else
                        saida.esforcoFase1Carga = NaN;
                        saida.cabo1SoloCarga = saida.cabo1SoloFonte;
                    end
                    saida.neutroSolo = saida.esforcoFase1Fonte - DimFixas.hfn2;
                else
                    saida.esforcoFase1Carga = Poste.Hlivre - DimFixas.hc;
                    saida.cabo1SoloCarga = Poste.Hlivre - DimFixas.hc;
                    saida.neutroSolo = saida.esforcoFase1Carga - DimFixas.hfn2;
                end

            else %caso so tenha cabos do lado da fonte
                saida.esforcoFase1Fonte = Poste.Hlivre - DimFixas.hf;
                saida.esforcoFase1Carga = NaN;
                saida.cabo1SoloCarga = NaN;
                if(~isnan(DimFixas.hpinof))
                    saida.cabo1SoloFonte = Poste.Hlivre - DimFixas.hf + DimFixas.hpinof + comp;
                else
                    saida.cabo1SoloFonte = Poste.Hlivre - DimFixas.hf;
                end
                saida.neutroSolo = saida.esforcoFase1Fonte - DimFixas.hfn1;
            end

            ref = saida.esforcoFase1Fonte;

            saida.hestlat = NaN;
            saida.hestlon = NaN;

            if ~isnan(DimFixas.hestlat)
                saida.hestlat = ref - DimFixas.hestlat;
            end
            if ~isnan(DimFixas.hestlon)
                saida.hestlon = ref - DimFixas.hestlon;
            end
            saida.esforcoFase2Carga = saida.esforcoFase1Carga;
            saida.esforcoFase2Fonte = saida.esforcoFase1Fonte;
            saida.esforcoFase3Carga = saida.esforcoFase1Carga;
            saida.esforcoFase3Fonte = saida.esforcoFase1Fonte;
            saida.cabo2SoloCarga = saida.cabo1SoloCarga;
            saida.cabo2SoloFonte = saida.cabo1SoloFonte;
            saida.cabo3SoloCarga = saida.cabo1SoloCarga;
            saida.cabo3SoloFonte = saida.cabo1SoloFonte;

        case 3 %script para o calculo do grupo3 de estruturas
            'nao � posivel ter derivacao no primeiro nivel';
        case 4 %script para o calculo do grupo3 de estruturas
            'nao � posivel ter derivacao no primeiro nivel';
        case 5
            saida.esforcoFase1Fonte = Poste.Hlivre - DimFixas.hf;
            saida.esforcoFase2Fonte = saida.esforcoFase1Fonte - DimFixas.hfn1;
            saida.esforcoFase3Fonte = saida.esforcoFase2Fonte - DimFixas.hfn2;
            %===================================================================
            % apesar de ter pino, ele fica na horizontal
            saida.cabo1SoloFonte = saida.esforcoFase1Fonte;
            saida.cabo2SoloFonte = saida.esforcoFase2Fonte;
            saida.cabo3SoloFonte = saida.esforcoFase3Fonte;

            if(~isnan(DimFixas.hc))  %descobre se tem cabos do lado da carga
                saida.esforcoFase1Carga = saida.esforcoFase1Fonte + DimFixas.hf - DimFixas.hc;
                saida.esforcoFase2Carga = saida.esforcoFase1Carga - DimFixas.hfn4;
                saida.esforcoFase3Carga = saida.esforcoFase2Carga - DimFixas.hfn5;

                saida.neutroSolo = saida.esforcoFase3Carga - DimFixas.hfn6;

                if(isequal(DimFixas.hpinoc,0))
                    saida.cabo1SoloCarga = saida.cabo1SoloFonte;
                    saida.cabo2SoloCarga = saida.cabo2SoloFonte;
                    saida.cabo3SoloCarga = saida.cabo3SoloFonte;
                    saida.esforcoFase1Carga = NaN;
                    saida.esforcoFase2Carga = NaN;
                    saida.esforcoFase3Carga = NaN;
                else
                    saida.cabo1SoloCarga = saida.esforcoFase1Carga;
                    saida.cabo2SoloCarga = saida.esforcoFase2Carga;
                    saida.cabo3SoloCarga = saida.esforcoFase3Carga;
                end

            else
                saida.esforcoFase1Carga = NaN;
                saida.cabo1SoloCarga = NaN;
                saida.esforcoFase2Carga = NaN;
                saida.cabo2SoloCarga = NaN;
                saida.esforcoFase3Carga = NaN;
                saida.cabo3SoloCarga = NaN;
                saida.neutroSolo = saida.esforcoFase3Fonte - DimFixas.hfn3;
            end
            %===================================================================
            if ~isnan(saida.esforcoFase1Fonte)
                ref = saida.esforcoFase1Fonte + DimFixas.hf;
            elseif ~isnan(saida.esforcoFase1Carga)
                ref = saida.esforcoFase1Fonte + DimFixas.hc;
            end

            saida.hestlat = NaN;
            saida.hestlon = NaN;

            if ~isnan(DimFixas.hestlat)
                saida.hestlat = ref - DimFixas.hestlat;
            end
            if ~isnan(DimFixas.hestlon)
                saida.hestlon = ref - DimFixas.hestlon;
            end
        case 6
             %é so repetir como se fosse uma do grupo 2
             saida = calcula_Nivel1(Poste,Nivel1,2);
        case 7
            saida.esforcoFase1Fonte = Poste.Hlivre - DimFixas.hf;
            saida.esforcoFase2Fonte = saida.esforcoFase1Fonte - DimFixas.hfn1;
            saida.esforcoFase3Fonte = saida.esforcoFase2Fonte;
            %===================================================================
            saida.cabo1SoloFonte = saida.esforcoFase1Fonte;
            saida.cabo2SoloFonte = saida.esforcoFase2Fonte;
            saida.cabo3SoloFonte = saida.esforcoFase3Fonte;

            saida.esforcoFase1Carga = saida.esforcoFase1Fonte;
            saida.esforcoFase2Carga = saida.esforcoFase2Fonte;
            saida.esforcoFase3Carga = saida.esforcoFase3Fonte;

            saida.cabo1SoloCarga = saida.cabo1SoloFonte;
            saida.cabo2SoloCarga = saida.cabo2SoloFonte;
            saida.cabo3SoloCarga = saida.cabo3SoloFonte;

            saida.neutroSolo = saida.esforcoFase3Carga - DimFixas.hfn2;

            %===================================================================
            ref = saida.esforcoFase1Fonte;
            saida.hestlat = NaN;
            saida.hestlon = NaN;

            if ~isnan(DimFixas.hestlat)
                saida.hestlat = ref - DimFixas.hestlat;
            end
            if ~isnan(DimFixas.hestlon)
                saida.hestlon = ref - DimFixas.hestlon;
            end
    end
    
    
    saidaT.estrutura = saida.estrutura;
    saidaT.tensao = saida.tensao;
    saidaT.pino = saida.pino;
    saidaT.preco = saida.preco;
    
    saidaT.hestlat = saida.hestlat;
    saidaT.hestlon = saida.hestlon;
    saidaT.neutroSolo = saida.neutroSolo;
    
    saidaT.cabo1SoloCarga = saida.cabo1SoloCarga;
    saidaT.cabo2SoloCarga = saida.cabo2SoloCarga;
    saidaT.cabo3SoloCarga = saida.cabo3SoloCarga;

    saidaT.cabo1SoloFonte = saida.cabo1SoloFonte;
    saidaT.cabo2SoloFonte = saida.cabo2SoloFonte;
    saidaT.cabo3SoloFonte = saida.cabo3SoloFonte;

    saidaT.esforcoFase1Carga = saida.esforcoFase1Carga;
    saidaT.esforcoFase2Carga = saida.esforcoFase2Carga;
    saidaT.esforcoFase3Carga = saida.esforcoFase3Carga;
    
    saidaT.esforcoFase1Fonte = saida.esforcoFase1Fonte;
    saidaT.esforcoFase2Fonte = saida.esforcoFase2Fonte;
    saidaT.esforcoFase3Fonte = saida.esforcoFase3Fonte;
    
end