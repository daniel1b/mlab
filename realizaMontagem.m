function saida = realizaMontagem(Montagem)
    % DEFINICOES - grupos para calculo
    % estruturas 'basicas' de uma fase
    G1 = {'U1' 'U2' 'U3' 'U4' 'U3-U3' 'U2-3'};
    % estruturas 'basicas' de tres fase
    G2 = {'N1' 'N2' 'N3' 'N4' 'N3-N3' 'N2-3' 'HT' 'HTE'};
    % estrutura de derivacao de uma fase
    G3 = {'UD-U1' 'UD-U2' 'UD-U4'};
    % estrutura de derivacao de tres fases
    G4 = {'ND-N1' 'ND-N2' 'ND-N3' 'ND-N4' 'UD-T'};
    % estrutura Ps
    G5 = {'P1' 'P1-A' 'P3' 'P4' 'P3-3'};
    % estruturas Beco e meiobeco
    G6 = {'B1' 'B2' 'B3' 'B4' 'B2-3' 'M1' 'M2' 'M3' 'M4'};
    % estrutura TE
    G7 = {'TE'};
    %tem 7 funcoes de calcular as dimensoes uma para cada grupo de
    %estrutura
    G = {G1 G2 G3 G4 G5 G6 G7};
    %--------------------------------------------------------------------------
    %===================Comeco da funcao======================
    %............em rela��o ao Poste .........................
    Poste.neutro = NaN;
    Poste.hestlat = NaN;
    Poste.hestlon = NaN;
    
    saida.Poste = Poste;
    saida.Trafo = NaN;
    saida.Baixa = NaN;
        
    saida.preco = 0;
    
    Poste.tamanho = Montagem.Poste.Hposte;
    Poste.posicao = Montagem.Poste.Posicao;
    Poste.engastamento = Poste.tamanho*0.1+0.6;
    Poste.Hlivre = Poste.tamanho - Poste.engastamento;
    
    %caracteristicasPoste = busca_poste(int2str(Montagem.Hposte), ...
    %   Montagem.resistencia, Montagem.material, Montagem.tipo)
    %............em relacao ao nivel 1........................
    for ic = 1:length(Montagem.AT)
        for contador = 1:length(G)
            if ismember(Montagem.AT(ic).estrutura,G{contador})
                grupo = contador;
            end
        end
        
        if isequal(ic,1)
            AT = calcula_Nivel1(Poste,Montagem.AT(ic),grupo);
        else
            AT = calcula_Nivel2(Poste,Montagem.AT(ic),grupo,saida.AT(ic-1));
        end
        
         Poste.neutro = AT.neutroSolo;
         Poste.hestlat = AT.hestlat;
         Poste.hestlon = AT.hestlon;
         
         AT = rmfield(AT, 'neutroSolo');
         AT = rmfield(AT, 'hestlat');
         AT = rmfield(AT, 'hestlon');
               
         saida.AT(ic) = AT;
         saida.preco = saida.preco + AT.preco;
         saida.Poste = Poste;
         
     end
     
     %=======================================================================
     if ischar(Montagem.Baixa.estrutura)
         Baixa = calcula_Secundario(Montagem.Baixa.estrutura,Poste);
         if isstruct(Baixa)
             Poste.neutro = Baixa.hn;
             Baixa = rmfield(Baixa, 'hn');
             saida.Baixa = Baixa;
             saida.preco = saida.preco + Baixa.preco;
             saida.Poste = Poste;
         end
     end

    if (Montagem.Trafo)
        saida.Trafo = calcula_trafo(Poste);
    end

end












