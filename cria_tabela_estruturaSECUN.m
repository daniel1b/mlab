clc; clear; close all;

Estrutura = {'A1' 'A2' 'A3' 'A4' 'A5' 'A1-D' 'A2-D' 'A3-D'...
    'A4-D' 'A5-D'};
campos = {'hn_normal' 'hn_topo' 'hfa_normal' 'hfa_topo' ...
    'hfb_normal' 'hfb_topo' 'hfc_normal' 'hfc_topo'...
    'hfd_normal' 'hfd_topo' 'preco'};
camposadd = {'estrutura'};

nomeArquivo = 'original.xlsx';
[dados,~,~] = xlsread(nomeArquivo,'Estrut. Secundárias', 'A1:P14');   

linhaAssoc = {5 6 7 8 9 10 11 12 13 14};

tabela = {};
for iEstr = 1:length(Estrutura)      
    
    linhaEst = linhaAssoc{iEstr};

    if isempty(tabela)
        linhaTAB = 1;
    else                
        linhaTAB = length(tabela(:,1))+1;
    end

    tabela{linhaTAB,1} = Estrutura{iEstr};            
    tabela{linhaTAB,2} = dados(linhaEst,6);
    tabela{linhaTAB,3} = dados(linhaEst,7);
    tabela{linhaTAB,4} = dados(linhaEst,8);
    tabela{linhaTAB,5} = dados(linhaEst,9);
    tabela{linhaTAB,6} = dados(linhaEst,10);
    tabela{linhaTAB,7} = dados(linhaEst,11);
    tabela{linhaTAB,8} = dados(linhaEst,12);
    tabela{linhaTAB,9} = dados(linhaEst,13);
    tabela{linhaTAB,10} = dados(linhaEst,14);
    tabela{linhaTAB,11} = dados(linhaEst,15);
    tabela{linhaTAB,12} = dados(linhaEst,16);
end

Tcabos = cell2table(tabela,'VariableNames',{camposadd{:} campos{:}});
writetable(Tcabos,'TABELA_EstrBAIXA.csv');







